% plots Fig. 6: Extinction probabilities when introducing ten virions of the second strain
% with varying inter-exposure intervals, for different values of
% 1/delta_L2;
% Fig. 7: Extinction probabilities when introducing ten virions of the second strain
% with varying inter-exposure intervals,for different values of 1/delta_I2
% Fig. 9: Extinction probabilities when introducing ten virions of the second strain
% with varying inter-exposure intervals, for different numbers of infectious stages
% Fig. 10: Extinction probabilities when introducing ten virions of the second strain
% with varying inter- exposure intervals, for different numbers of
% infectious stages. Parameters are as per Fig. 9 except q1 = 5 � 10?6
% Fig. 11: Extinction probabilities when introducing ten virions of the second strain
% with varying inter- exposure intervals, for different numbers of latent stages 
% input: fig_no: scalar: figure no. to plot
% input: simulate: logical: 1 = plot results of simulations, 0 =
% plot_calculated values only
% input: presimulated: logical: 1 = use pre-simulated values, 0 =
% re-simuate values
% output: p_extinct: vector: extinction probabilities  for one virion across quantity
% varied

function p_extinct = plot_figs6791011(fig_no,simulate,presimulated)
t = 0:.25:14; % inter-exposure intervals for which to calculate extinction probability
t_e = 50; % time at which integration ends for numerical method
n_virions = 10;m % number of initial virions
styles = {'-k','--k',':k','-.k','k*','k+','kx'};

[p,initial_values] = setup_common_parameters();
t_simulated = 1:12; % inter-exposure intervals for which to simulate extinction probability
if(simulate && presimulated) % read in pre-simulated values if applicable
    extinction_probability = csvread(strcat('extinction_probability_fig',num2str(fig_no),'.dat'));
end

switch fig_no
    case 6
        legend_str = {'0.2 days','0.4 days','0.6 days','0.8 days','1.0 days'};
    case 7
        legend_str = {'0.2 days','0.5 days','1.0 days','1.5 days','2.0 days','2.5 days','3.0 days'};
    otherwise
        legend_str = {'1','2','3','4','5','6','7'};
end

switch fig_no
    case 6
        vec = .2:.2:1; % numbers of latent stages
        p.K = 1;
        p.M = 1;
        p.delta_I2 = 3;
    case 7
        vec = [.2,.5:.5:3]; % values of 1/delta_I2
        p.K = 1;
        p.M = 1;
        p.delta_L2 = 3;
    case 9
        vec = 1:7; % number of infectious stages
        p.K = 1;
        p.delta_L2 = 3;
        p.delta_I2 = 3;
    case 10
        vec = 1:7; % number of infectious stages
        p.q1 = 5e-6;
        p.K = 1;
        p.delta_L2 = 3;
        p.delta_I2 = 3;
    case 11
        vec = 1:7; % number of latent stages
        p.M = 1;
        p.delta_L2 = 3;
        p.delta_I2 = 3;
end


for j = 1:length(vec)
    switch fig_no
        case 6
            p.delta_L2 = 1/vec(j);
        case 7
            p.delta_I2 = 1/vec(j);
        case 9
            p.M = vec(j);
        case 10
            p.M = vec(j);
        case 11
            p.K = vec(j);
    end        
    sol = calc_extinction_probability_time_dependent(p,initial_values); % calculate extinction probability
    p_extinct = deval(sol,t_e-t,1);
    if(simulate)
        if(presimulated)
            p_extinct_simulated = extinction_probability(:,j);
        else
            for i = 1:length(t_simulated)
                p_extinct_simulated(i) = simulate_extinction_probability(p,initial_values,t_simulated(i),n_virions); % simulate extinction probability
            end
        end
        figure(j)
        plot_calc_vs_sim(t,p_extinct.^n_virions,t_simulated,p_extinct_simulated);
        xlabel('Inter-exposure interval (days)')
        ylabel('Extinction probability')
        title(legend_str{j})
        axis([0 14 0 1])
        
        save_fig(strcat('Fig',num2str(fig_no),'_',num2str(j)),8)        
    else
        if(j<=4)
            plot(t,p_extinct.^10,styles{j});
        else
            plot(t,p_extinct.^10,styles{j},'MarkerSize',2);
        end
        hold on;
    end
end

if(simulate == 0)
    hold off
    xlabel('Inter-exposure interval (days)')
    ylabel('Extinction probability')
    legend(legend_str)        
    axis([0 14 0 1])
    save_fig(strcat('Fig',num2str(fig_no)),8)
end