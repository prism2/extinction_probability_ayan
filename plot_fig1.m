% plots Fig. 1 in manuscript: Extinction probability when introducing ten virions, 
% varying the mean infectious period 1/delta_I and
% the number of infectious stages M.
% output: rho_V: vector containing extinction probability when introducing
% one virion
% output: one_on_delta_I_vector: vector of 1/delta_I for which rho_V is
% calculated
function [rho_V,one_on_delta_I_vector] = plot_fig1()

[p,~] = setup_common_parameters(); % default parameter values

one_on_delta_I_vector = .1:.1:4;
delta_I_vector = 1./one_on_delta_I_vector; % values of delta_I for which to calculate extinction probability
rho_V = zeros(length(delta_I_vector),7);
p.K = 0;

for m = 1:7
    p.M = m; % number of infectious stages
    for j = 1:length(delta_I_vector)
        p.delta_I2 = delta_I_vector(j);
        [rho_V(j,m),~,~] = calc_extinction_probability_tiv_stages(p); % calculate the extinction probability when starting from one virion
    end
end

% plot results
styles = {'-k','--k',':k','-.k','k*','k+','kx'};
n_virions = 10; % calculating extinction probability for 10 virions
for m = 1:7
    if(m<=4)
        semilogy(one_on_delta_I_vector,rho_V(:,m).^n_virions,styles{m});
    else
        semilogy(one_on_delta_I_vector,rho_V(:,m).^n_virions,styles{m},'MarkerSize',2); % increase marker size for these markers, for visibility
    end
    hold on;
end
hold off
h = xlabel('1/\delta_{\fontname{Times}{\it{I}}} \fontname{Arial}{(days)}');
set(h,'Interpreter','tex')
ylabel('Extinction probability of 10 virions')
legend('1','2','3','4','5','6','7')
save_fig('delta_stages_10_virions',8);