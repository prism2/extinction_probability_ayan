% plots all figures in manuscript.  Uses pre-simulated results
function plot_all()

plot_fig1();
plot_fig3();
presimulated = 1;
plot_fig4(presimulated);
plot_fig5(presimulated);
for i = [6,7,9,10,11]
    plot_figs6791011(i,1,presimulated);
end
plot_fig8();

end
