% calculate the extinction probability for the TIV model when there are multiple
% latent cell adn infectious cell stages, for one initial virion (rho_V), one
% initial latent cell (rho_L), and one initial infectious cell (rho_I)
function [rho_V,rho_L,rho_I] = calc_extinction_probability_tiv_stages(p)

rho_I = zeros(1,p.M); % initialise array
p.gamma2 = p.beta2*p.T_0/(p.beta2*p.T_0+p.c2); % the probability that a virion infects a cell before decaying
p.R_0 = p.gamma2*p.p2/p.delta_I2; % basic reproduction number

if(p.R_0 <= 1) % if R_0 < 1, all extinction probabilities are equal to 1
    rho_V = 1;
    rho_L = ones(1,p.K);
    rho_I = ones(1,p.M);
    
else
    eqn = @(rho_I) (p.p2 + p.M*p.delta_I2 - p.p2*(p.gamma2*rho_I + 1-p.gamma2))^p.M*rho_I - (p.M*p.delta_I2)^p.M; % solving Eq. 13a and 14 simultaneously for rho_I
    rho_I(1) = fzero(eqn,0);
    rho_V = (p.gamma2*rho_I(1) + 1-p.gamma2); % substituting rho_I(1) into Eq. 13a to obtain rho_V
    rho_L = rho_I(1)*ones(1,p.K); % substituting rho_I(1) into Eq. 13b to obtain rho_L

    for i = 2:p.M
        rho_I(i) = rho_I(1)^((p.M-i)/p.M); % substituting rho_I(1) into Eq. 14 to obtain rho_I(i)
    end
end

end
