% calculate the extinction probability for the model with a time-dependnet
% immune response
% input: p: struct of parameter values constructed using
% setup_common_parameters.m
% initial_values: vector of initial conditions constructed using
% setup_common_parameters.m
% output: sol2: struct in the format outputted by Matlab ODE solvers:
% solution of Eq. 21
function sol2 = calc_extinction_probability_time_dependent(p,initial_values)

endpoint = 50; %t_e
timespan = [0,endpoint];

options=odeset('NonNegative',1:length(initial_values));
% solve deterministic ODE up until inter-exposure interval
sol = ode15s(@(tdum,xdum) deterministic_ode(tdum,xdum,p,0,0),timespan,initial_values,options);

options = odeset('NonNegative',1:1+p.K+p.M,'AbsTol',1e-12,'RelTol',1e-6);

% final conditions for Eq. 21 (as per Eq. 24)
[rho_V,rho_L,rho_I] = calc_extinction_probability_tiv_stages(p);
initial_values = [rho_V,rho_L,rho_I];

% solve Eq. 21
sol2 = ode15s(@(tdum,xdum) stochastic_extinction_ode(tdum,xdum,p,sol),timespan,initial_values,options);
end


