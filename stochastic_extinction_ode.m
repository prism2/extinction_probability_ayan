% rhs for Eq. 21 (ODE to calculate extinction probability when there is a
% time-dependent immune response)
% input: t: scalar: time at which to evaluate rhs
% input: x: vector: current state of system
% input: p: struct of parameter values constructed uisng
% setup_common_parameters.m
% input: sol: struct of form returned by Matlab ODE solvers: trajectory of
% time-dependent immune response
function xdot = stochastic_extinction_ode(t,x,p,sol)

V = 1;
L = 2:1+p.K;
I = 2+p.K:1+p.K+p.M;
xdot = zeros(I(end),1);
T = deval(sol,sol.x(end)-t,2); % extract time-dependent number of target cells
F = deval(sol,sol.x(end)-t,4); % extract time-dependent amount of interferon

if(p.K == 0) % equations when there is no latent stage
    xdot(V) = p.beta2*T*(x(V) - x(I(1))) + p.c2*(x(V) - 1);
else
    xdot(V) = p.beta2*T*(x(V) - x(L(1))) + p.c2*(x(V) - 1); % equations when there is a latent stage
    xdot(L(1:end-1)) = p.K*p.delta_L2*(x(L(1:end-1)) - x(L(2:end)));
    xdot(L(end)) = p.K*p.delta_L2*(x(L(end)) - x(I(1)));
end
xdot(I(1:end-1)) = p.M*p.delta_I2*(x(I(1:end-1)) - x(I(2:end))) +p.p2/(1+p.s2*F)*x(I(1:end-1))*(1-x(V));
xdot(I(end)) = p.M*p.delta_I2*(x(I(end)) - 1) +p.p2/(1+p.s2*F)*x(I(end))*(1-x(V));

xdot = -xdot;

end
