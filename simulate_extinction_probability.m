% obtain extinction probability by simulation for one intiial virion of strain 2 for
% model with time-dependent immune response
% input argument: p: structure containing default parameters, set up using
% setup_common_parameters.m
% input argument: initial_values: vector containing initial values, set up using
% setup_common_parameters.m
% input argument: interval: scalar: inter-exposure interval for which to
% obtain extinction probability
% input_argument: V2_init: scalar: initial number of virions for strain 2
function extinction_probability = simulate_extinction_probability(p,initial_values,interval,V2_init)

rng('shuffle');

options = odeset('NonNegative',1:length(initial_values));
[~,x] = ode15s(@deterministic_ode,[0 interval],initial_values,options,p,0,zeros(p.M,1)); % solve deterministic ODE up until exposure to second strain

total_reps = 1000; % number of replicates
extinction = zeros(total_reps,1);

for i = 1:total_reps
    extinction(i) = tau_leap_stages(x(end,:),V2_init,p); % simulate viral dynamics after introduction of the second virus using tau-leap method, and return whether each ralisation results in extinction
end

extinction_probability = sum(extinction)/total_reps; % calculate extinction probability

end

