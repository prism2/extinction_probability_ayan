% evaluates the right-hand side of model ODEs for the model with a
% time-dependent immune response (Eq. 22),
% given the number of virions and number of infectious cells for strain 2 (V2 and I2)
function xdot=deterministic_ode(~,x,p,V2,I2)
V1 = 1; % compartment labelling
T = 2;
I1 = 3;
F = 4;
B1 = 5;
A1 = 6;

D=(p.T_0-x(T)-x(I1)-sum(I2))/p.T_0; % target cell regrowth rate

xdot=zeros(6,1);
xdot(V1)=p.p1/(1+p.s1*x(F))*x(I1)-p.c1*x(V1)-p.mu11*x(V1)*x(A1)-p.beta1*x(V1)*x(T);
xdot(T)=p.g*D*x(T)-p.beta1*x(V1)*x(T)-p.beta2*V2*x(T);
xdot(I1)=p.beta1*x(V1)*x(T)-p.delta1*x(I1);
xdot(F)=(p.q1*x(I1)+p.q2*sum(I2))-p.d*x(F);
xdot(B1)=p.m11*x(V1)*(1-x(B1))-p.m21*x(B1);
xdot(A1)=p.m31*x(B1)-p.mu21*x(V1)*x(A1)-p.r1*x(A1);

end
