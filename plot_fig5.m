% plots Fig. 4 in manuscript: 
% Calculated versus simulated extinction probabilities when introducing 
% different numbers of virions of the second strain 
% with a 3.5-day inter-exposure interval. 
% input argument: presimulated: logical.  Whether to simulate extinction
% probabilities or use pre-simulated results.
function plot_fig5(presimulated)

[p,initial_values] = setup_common_parameters();
p.K = 0;
p.M = 1;
p.delta_L2 = 0;
p.delta_I2 = 3;
IEI = 3.5; % inter-exposure interval
t_e = 50; % time at which to stop integrating for numerical method

x_simulated = 200:200:2000; % number of initial virions for strain 2
p_extinct_simulated = [.804,.661,.553,.443,.342,.296,.246,.178,.157,.127];
if(presimulated == 0) % re-simulate results if necessary
    for i = 1:length(x_simulated)
        p_extinct_simulated = simulate_extinction_probability(p,initial_values,IEI,x_simulated(i));
    end
end
    
x = 0:2000;
sol = calc_extinction_probability_time_dependent(p,initial_values); % calculate extinction probability for one virion using Eq. 21
p_extinct = deval(sol,t_e-IEI,1);
p_extinct = p_extinct.^x; % calculate extinction probability for x virions

% plot results
plot_calc_vs_sim(x,p_extinct,x_simulated,p_extinct_simulated);
axis([0 2000 0 1])
xlabel('Number of virions')
save_fig('Fig5',8)