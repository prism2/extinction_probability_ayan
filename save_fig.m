function save_fig(filename,width)

ax = gca;
fig = gcf;

ax.FontSize = 8;
ax.Units = 'centimeters';
fig.Units = 'centimeters';
fig.PaperUnits = 'centimeters';
x = fig.Position;
fig.Position = [0,0,fig.Position(3:4)].*width/x(3);
left_text = ax.TightInset(1);
bottom_text = ax.TightInset(2);
left_right_text = ax.TightInset(1)+ax.TightInset(3);
top_bottom_text = ax.TightInset(2)+ax.TightInset(4);
ax.OuterPosition = fig.Position;
ax.Position = [left_text,bottom_text,fig.Position(3)-left_right_text,fig.Position(4)-top_bottom_text];
fig.PaperPosition = fig.Position;
print(filename,'-deps')
end