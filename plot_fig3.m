% plot Figure 3 in manuscript: The number of target cells, T(t), and the
% amount of type I inferferon, F(t).
function plot_fig3()

[p,initial_values] = setup_common_parameters(); % default parameter values

timespan = [0,14]; % timespan over which to integrate model equations

options=odeset('NonNegative',1:length(initial_values)); % positive values for all model compartments

[t,x] = ode15s(@(tdum,xdum) deterministic_ode(tdum,xdum,p,0,0),timespan,initial_values,options); % integrate the model equations

subplot(1,2,1)
plot(t,x(:,2),'k')
ax1 = gca;
axis([0 14 0 7e7])
xlabel('Time (days)')
ylabel('No. of target cells')

subplot(1,2,2)
plot(t,x(:,4),'k')
ax2 = gca;
axis([0 14 0 1])
xlabel('Time (days)')
ylabel('Type I interferon')

fig = gcf;
width = 12.2;
height = width/2*3/4;
ax1.FontSize = 8;
ax2.FontSize = 8;
ax1.XTick = 0:2:14;
ax2.XTick = 0:2:14;
ax1.Units = 'centimeters';
ax2.Units = 'centimeters';
fig.Units = 'centimeters';
fig.PaperUnits = 'centimeters';
fig.Position = [0,0,width,height];
left_text1 = ax1.TightInset(1);
left_text2 = ax2.TightInset(1);
bottom_text = ax1.TightInset(2);
left_right_text1 = ax1.TightInset(1)+ax1.TightInset(3);
left_right_text2 = ax2.TightInset(1)+ax2.TightInset(3);
width_figure = (width-left_right_text1-left_right_text2)/2;
top_bottom_text = ax1.TightInset(2)+ax1.TightInset(4);
ax1.OuterPosition = [0,0,left_right_text1+width_figure,height];
ax2.OuterPosition = [left_right_text1+width_figure,0,left_right_text2+width_figure,height];
ax1.Position = [left_text1,bottom_text,width_figure,height-top_bottom_text];
ax2.Position = [left_right_text1+width_figure+left_text2,bottom_text,width_figure,height-top_bottom_text];
fig.PaperPosition = fig.Position;
print('Fig3','-deps')