% function to simulate stochastic dynamics of V2 and I2
% input: deterministic_init: vector: initial values of deterministic compartments
% input: V2_init: scalar: initial number of virions for strain 2
% input: p: structure: parameter values, set up using
% setup_common_parameters.m
% output: extinction: logical: whether extinction occurred for this
% realisation
function extinction = tau_leap_stages(deterministic_init,V2_init,p)

dt=0.001; % time step size for each iteration
time = 0:dt:10; % integrate up to 10 days

current_state = deterministic_init; % start simulation from initial values of deterministic compartments

V2num = V2_init; % V2num records the time series of free V2 numbers
L2num = zeros(p.K,1);
I2num = zeros(p.M,1);
r = zeros(3+p.K+p.M,1); % initialise vector of the number of events at each time step

options = odeset('NonNegative',1:length(current_state));

for iter=1:length(time)-1
    T = current_state(2);
    F = current_state(4);
    
    % number of virion death events
    r(1) = poissrnd(dt*p.c2*V2num);
    % number of infection events
    r(2) = poissrnd(dt*p.beta2*T*V2num);
    % number of latent cell moving to next stage events
    r(3:2+p.K) = poissrnd(dt*p.delta_L2*p.K*L2num);
    % number of infected cell moving to next stage events
    r(3+p.K:2+p.K+p.M) = poissrnd(dt*p.delta_I2*p.M*I2num);
    % number of virion production events
    r(3+p.K+p.M) = sum(poissrnd(dt*p.p2/(1+p.s2*F)*I2num));

    [~,y] = ode15s(@deterministic_ode,[0 dt],current_state,options,p,V2num,I2num); % solve deterministic ODE for the deterministic compartments
    current_state = y(end,:);

    V2num = max(V2num - r(1) - r(2) + r(3+p.K+p.M),0); % update the number of V2
    L2num = max(L2num +r(2:1+p.K) - r(3:2+p.K),0); % update the number of L2
    I2num = max(I2num + r(2+p.K:1+p.K+p.M) - r(3+p.K:2+p.K+p.M),0); % update the number of I2
    
    if (V2num >= 10000 || V2num+sum(I2num)+sum(L2num) == 0) % take-off is defined as having over 10^4 virions; extinction is defined as having zero virions and infected cells
        break
    end
end

extinction = V2num+sum(I2num)+sum(L2num) == 0; % determine whether extinction occurred

end
