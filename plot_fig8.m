% plots Fig. 8 in manuscript: Extinction probabilities when introducing ten virions
% of the second strain with a 3.5-day inter- exposure interval,
% for different numbers of 1/delta_L2 and 1/delta_I2 as indicated by the x-
% and y-axes, when there is one latent stage and one infectious stage.

function plot_fig8()
one_on_delta_L = .05:.1:1.05; % values of 1/delta_L2 for which to calculate extinction probability
one_on_delta_I = .1:.1:4; % values of 1/delta_I2
p_extinct = zeros(length(one_on_delta_L),length(one_on_delta_I)); % initialise matrix to store results

[p,initial_values] = setup_common_parameters();
p.K = 1;
p.M = 1;
IEI = 3.5; % inter-exposure interval
t_e = 50; % time to stop integrating

for i = 1:length(one_on_delta_L)
    for j = 1:length(one_on_delta_I)
        p.delta_L2 = 1/one_on_delta_L(i);
        p.delta_I2 = 1/one_on_delta_I(j);
        sol = calc_extinction_probability_time_dependent(p,initial_values); % calculate extinction probability
        p_extinct(i,j) = deval(sol,t_e-IEI,1);
    end
end

n_virions = 10; % initial number of virions

colormap(flipud(gray));
C = contour(one_on_delta_L,one_on_delta_I,p_extinct.^n_virions');
caxis([-.2,1])
clabel(C,'FontSize',8);
x = xlabel('1/\delta_{\fontname{Times}{\it{L}}} \fontname{Arial}{(days)}');
y = ylabel('1/\delta_{\fontname{Times}{\it{I}}} \fontname{Arial}{(days)}');
set(x,'Interpreter','tex');
set(y,'Interpreter','tex');
axis([.1 1 0 4]);
save_fig('extinction_probability_delta_L_delta_I',8);