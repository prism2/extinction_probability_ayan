function plot_calc_vs_sim(x,p_extinct,x_simulated,p_extinct_simulated)

uu = binoinv(.975,1000,p_extinct)/1000;
lu = binoinv(.025,1000,p_extinct)/1000;
h = area(x,vertcat(lu,uu-lu)','LineStyle','none');
% h = area(x,vertcat(1-uu,uu-lu)','LineStyle','none');
set(h(1),'FaceColor','w');
set(h(2),'FaceColor',[192,192,192]/255);
hold on
h(3) = plot(x_simulated,p_extinct_simulated,'.k','MarkerSize',10);
h(4) = plot(x,p_extinct,'k');
% h(3) = plot(x_simulated,1-p_extinct_simulated,'.k','MarkerSize',20);
% h(4) = plot(x,1-p_extinct,'k','LineWidth',2);
hold off
legend(h(3:4),'simulated','calculated')
ylabel('Extinction probability')
set(gca,'Layer','Top')


