% function to set up structures with default parameter values and initial
% conditions
% output: p: structure containing parameter values
% output: initial_values: vector contaiing initial conditions
function [p,initial_values] = setup_common_parameters()

p.beta1 = 5e-7;
p.delta1 = 3;
p.p1 = 14;
p.c1 = 20;

p.T_0 = 7e7;
p.g = .8;

p.q1 = 1e-7;
p.q2 = 5e-6;
p.d = 2;

p.s1 = 1;

p.m11 = 2.5e-6;
p.m21 = .01;
p.m31 = 12000;
p.mu11 = .2;
p.mu21 = 1e-3;
p.r1 = 0.2;

p.beta2 = p.beta1;
p.p2 = p.p1;
p.c2 = p.c1;
p.s2 = p.s1;

iv.T_0 = p.T_0;
iv.F_0 = 0;
iv.I1_0 = 0;
iv.V1_0 = 10;
iv.A1_0 = 0;
iv.B1_0 = 0;

initial_values = [iv.V1_0,iv.T_0,iv.I1_0,iv.F_0,iv.B1_0,iv.A1_0];

end