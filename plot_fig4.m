% plots Fig. 4 in manuscript: 
% Calculated versus simulated extinction probabilities when introducing ten
% virions of the second strain a number of days after primary inoculation 
% (the inter-exposure interval).
% input argument: presimulated: logical.  Whether to simulate extinction
% probabilities or use pre-simulated results.

function plot_fig4(presimulated)

[p,initial_values] = setup_common_parameters(); % default parameter values
p.K = 0; % no latent stage
p.M = 1; % one infectious stage
p.delta_L2 = 0;
p.delta_I2 = 3;
n_virions = 10; % initial number of virions for second strain

t_simulated = [1,2,2.25,2.5,2.75,3,3.25,3.5,3.75,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10,10.5,11]; % times for which to simulate extinction probability
p_extinct_simulated = [.003,.121,.423,.76,.907,.967,.987,.982,.99,.988,.973,...
        .944,.844,.678,.471,.252,.159,.066,.048,.02,.01,.01,.004,.006]; % presimulated extinction probabilities
if(presimulated == 0)
    for i = 1:length(t_simulated)
        p_extinct_simulated(i) = simulate_extinction_probability(p,initial_values,t_simulated(i),n_virions); % re-simulate extinction probabilities if desired
    end
end

t = 0:.05:14; % calculate extinction probability according to Eq. 21 for these inter-exposure intervals
t_e = 50; % time at which infection is deemed to be over (end of integration period)
sol = calc_extinction_probability_time_dependent(p,initial_values);
p_extinct = deval(sol,t_e-t,1); % extinction probability for one initial virion
p_extinct = p_extinct.^n_virions;

% plot results
plot_calc_vs_sim(t,p_extinct,t_simulated,p_extinct_simulated)
axis([0 14 0 1])
xlabel('Inter-exposure interval (days)')
save_fig('Fig4',8)


